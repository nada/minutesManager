var mMan = {

    //PROPERTIES
    listView: null,
    minList: null,
    toAddView: null,
    toListView: null,
    addView: null,
    saveBtn: null,
    minutesInp: null,
    minDateInp: null,
    minTopicsInp: null,
    searchInp: null,
    minutes: [],
    searchResults: [],



    //FUNCTIONS

    //initialize app
    init: () => {
        //fill properties
        mMan.listView = document.getElementById("listView");
        mMan.minList = document.getElementById("list");
        mMan.addView = document.getElementById("addView");
        mMan.addView.classList.add("ninja");
        mMan.toAddView = document.getElementById("toAddView");
        mMan.toAddView.onclick = () => {
            mMan.listView.classList.add("ninja");
            mMan.addView.classList.remove("ninja");
            document.getElementById("alert").classList.add("ninja");
        };
        mMan.toListView = document.getElementById("toListView");
        mMan.toListView.onclick = () => {
            mMan.addView.classList.add("ninja");
            mMan.listView.classList.remove("ninja");
            mMan.topics = [];
        };
        mMan.saveBtn = document.getElementById("saveBtn");
        mMan.saveBtn.onclick = mMan.save;
        mMan.minutesInp = document.getElementById("minLink");
        mMan.minDateInp = document.getElementById("minDate");
        mMan.minTopicsInp = document.getElementById("minTopics");
        mMan.searchInp = document.getElementById("searchInp");
        mMan.searchInp.oninput = mMan.search;


        //listen to updates
        window.webxdc.setUpdateListener(function (update) {
            if (update.payload.addition) { //addition to the minutes
                let obj = {
                    id: update.payload.id,
                    date: update.payload.date,
                    creator: update.payload.creator,
                    link: update.payload.link,
                    topics: update.payload.topics,
                };

                mMan.minutes.unshift(obj);
            } else {             //deletion from the minutes
                let index = mMan.minutes.findIndex((obj) => {
                    return Number.parseInt(obj.id) === Number.parseInt(update.payload.id);
                });
                if (index != -1) mMan.minutes.splice(index, 1);
            }
            mMan.list();
        });

        mMan.list();
    },

    //save a minutes
    save: () => {
        let link = mMan.minutesInp.value;
        let date = mMan.minDateInp.value;
        let info = window.webxdc.selfName + " saved a minutes from " + date;
        let id = Date.now();
        let topicsVal = mMan.minTopicsInp.value;
        let topics = topicsVal.split(",");
        for (let index in topics) {
            topics[index] = topics[index].trim();
        }

        //form validation
        if(date == ""||topics.length == 0|| link == ""){
            document.getElementById("alert").classList.remove("ninja");
            return;
        }

        //send update
        window.webxdc.sendUpdate(
            {
                payload: {
                    id: id,
                    date: date,
                    addition: true,
                    creator: window.webxdc.selfName,
                    link: link,
                    topics: topics
                },
                info,
            },
            info
        );
    },

    //list the minutes
    list: (minutes = mMan.minutes) => {
        //clean container content
        mMan.minList.innerHTML = "";

        //display the list view
        mMan.listView.classList.remove("ninja");
        mMan.addView.classList.add("ninja");

        if (minutes.length == 0) {
            mMan.minList.innerHTML = "<p>There's no minutes yet</p>";
        } else {

            //append the minutes
            for (let min of minutes) {
                let div = document.createElement("div");
                let date = document.createElement("h3");
                let link = document.createElement("a");
                let deletion = document.createElement("span");
                let topics = document.createElement("p");

                for (let topic of min.topics) {
                    topics.innerHTML += "<span>" + topic + "</span>";
                }
                deletion.setAttribute("id-data", min.id);
                deletion.innerHTML = '<svg id="i-close" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" width="15" height="15" fill="none" stroke="currentcolor" stroke-linecap="round" stroke-linejoin="round" stroke-width="3"><path d="M2 30 L30 2 M30 30 L2 2" /></svg>';
                deletion.onclick = mMan.delete;

                link.setAttribute("href", min.link);
                div.setAttribute("data-id", min.id);
                div.setAttribute("class", "minute");

                date.textContent = min.date;

                div.appendChild(deletion);
                link.appendChild(date);
                div.appendChild(link);
                div.appendChild(topics);

                mMan.minList.appendChild(div);
            }
        }
    },

    //delete the minutes
    delete: (evt) => {
        console.log(evt.currentTarget);
        let info = window.webxdc.selfName + " deleted a minutes";
        //send update
        window.webxdc.sendUpdate(
            {
                payload: {
                    id: evt.currentTarget.getAttribute("id-data"),
                    addition: false,
                    creator: window.webxdc.selfName,
                },
                info,
            },
            info
        );
    },

    //display results by topic 
    search: () => {
        //get the input
        let topic = mMan.searchInp.value;
        let topicExists = false;
        //clean the search results
        mMan.searchResults = [];

        //if no input just list the minutes
        if (topic == "") {
            mMan.list();
        } else {
            for (let min of mMan.minutes) {
                if (min.topics.includes(topic)) {
                    mMan.searchResults.unshift(min);
                    topicExists = true;
                }
            }
            //display the results with list()
            if (topicExists) {
                mMan.list(mMan.searchResults);
            } else {
                mMan.list();
            }
        }
    }




};
window.addEventListener("load", mMan.init);
